export class Element {
    private _id: number
    public get id(): number {
        return this._id
    }
    public set id(v: number) {
        this._id = v
    }

    private _name: string
    public get name(): string {
        return this._name
    }
    public set name(v: string) {
        this._name = v
    }

    private _shortName: string
    public get shortName(): string {
        return this._shortName
    }
    public set shortName(v: string) {
        this._shortName = v
    }

    private _selected: boolean
    public get selected(): boolean {
        return this._selected
    }
    public set selected(v: boolean) {
        this._selected = v
    }

    constructor(id: number, name: string) {
        this.id = id
        this.name = name
    }
}
