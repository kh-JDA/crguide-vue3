import { fireEvent, render, screen } from "@testing-library/vue"
import "@testing-library/jest-dom"

import SearchBox from "../../components/SearchBox.vue"

describe("SearchBox props", () => {
    test("cssClasses prop renders css classes to classlist", async () => {
        // Arrange
        const testCssClass = "testClass"
        const testCssClass2 = "testingClass2"
        const { container } = render(SearchBox, {
            props: {
                modelValue: "",
                cssClasses: [testCssClass, testCssClass2],
            },
        })

        // Act

        // Assert
        expect(container.children[0].classList).toContain(testCssClass)
        expect(container.children[0].classList).toContain(testCssClass2)
    })

    test("modelValue prop renders to input value", async () => {
        // Arrange
        const value = "testString"

        const { getByDisplayValue } = render(SearchBox, {
            props: {
                modelValue: value,
                cssClasses: [],
            },
        })

        // Act

        // Assert
        expect((getByDisplayValue(value) as HTMLInputElement).value).toEqual(value)
    })

    test("clear button is hidden with no input", async () => {
        // Arrange
        render(SearchBox, {
            props: {
                modelValue: "",
                cssClasses: [],
            },
        })

        // Act

        // Assert
        expect(screen.queryByRole("button")).not.toBeInTheDocument()
    })

    test("clear button is visible with input", async () => {
        // Arrange
        render(SearchBox, {
            props: {
                modelValue: "test",
                cssClasses: [],
            },
        })

        // Act

        // Assert
        expect(screen.queryByRole("button")).toBeInTheDocument()
    })
})

describe("SearchBox events", () => {
    // Arrange
    const event = "update:modelValue"

    test("searchbox emits the correct input value", async () => {
        // Arrange
        const testString = "testString"
        const { emitted } = render(SearchBox, {
            props: {
                modelValue: "",
                cssClasses: [],
            },
        })
        const input = screen.getByRole("searchbox") as HTMLInputElement

        // Act
        input.value = testString
        await fireEvent.input(input)

        // Asserts
        const emittedEvents = emitted()

        // assert event has been emitted
        expect(
            // @ts-ignore
            emittedEvents[event]
        ).toBeTruthy()

        // assert event payload
        expect(
            // @ts-ignore
            (emittedEvents[event][0] as string[])[0]
        ).toEqual(testString)
    })

    test("searchbox emits empty when clicking x", async () => {
        // Arrange
        const { emitted } = render(SearchBox, {
            props: {
                modelValue: "test",
                cssClasses: [],
            },
        })
        const btn = screen.getByRole("button")

        // Act
        await fireEvent.click(btn)

        // Assert
        const emittedEvents = emitted()

        // assert event has been emitted
        expect(
            // @ts-ignore
            emittedEvents[event]
        ).toBeTruthy()

        // assert event payload
        expect(
            // @ts-ignore
            (emittedEvents[event][0] as string[])[0]
        ).toEqual("")
    })
})
