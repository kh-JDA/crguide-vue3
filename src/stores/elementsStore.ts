import { Store } from "./main"
import { Element } from "../classes/Element"

import { MaterialService } from "../services/materialService"
import { MediumService } from "../services/mediumService"
import { ElementService } from "../services/elementService"
import { IElementSelectedModel } from "../definitions/IElementSelectedModel"
import { IRawMaterial } from "../definitions/IRawMaterial"
import { IRawMedium } from "../definitions/IRawMedium"

interface Elements extends Object {
    materials_all: Element[]
    media_all: Element[]
}

class ElementsStore extends Store<Elements> {
    protected data(): Elements {
        return {
            materials_all: [],
            media_all: [],
        }
    }

    // MATERIALS

    async getMaterials(): Promise<void> {
        const rawMaterials = await this.fetchMaterials()
        let result = MaterialService.normalizeMaterials(rawMaterials)
        result = result.sort((a, b) => a.name.localeCompare(b.name))
        this.state.materials_all = result
    }

    async fetchMaterials(): Promise<IRawMaterial[]> {
        const response = await fetch("https://resistanceguide.azurewebsites.net/api/Material/Get")
        const data: IRawMaterial[] = await response.json()
        return data
    }

    selectAllMaterials(): void {
        ElementService.changeSelectionOfAllElements(this.state.materials_all, true)
    }
    deselectAllMaterials(): void {
        ElementService.changeSelectionOfAllElements(this.state.materials_all, false)
    }
    changeMaterialSelection(payload: IElementSelectedModel): void {
        ElementService.changeSelectedValueOfElement(this.state.materials_all, payload.element.id, payload.selected)
    }
    getSelectedMaterials(): Element[] {
        return ElementService.getSelectedElements(this.state.materials_all)
    }

    // MEDIA

    async getMedia(): Promise<void> {
        const rawMedia = await this.fetchMedia()
        let result = MediumService.normalizeMedia(rawMedia)
        result = result.sort((a, b) => a.name.localeCompare(b.name))
        this.state.media_all = result
    }

    async fetchMedia(): Promise<IRawMedium[]> {
        const response = await fetch("https://resistanceguide.azurewebsites.net/api/Medium/Get")
        const data: IRawMedium[] = await response.json()
        return data
    }

    selectAllMedia(): void {
        ElementService.changeSelectionOfAllElements(this.state.media_all, true)
    }
    deselectAllMedia(): void {
        ElementService.changeSelectionOfAllElements(this.state.media_all, false)
    }
    changeMediumSelection(payload: IElementSelectedModel): void {
        ElementService.changeSelectedValueOfElement(this.state.media_all, payload.element.id, payload.selected)
    }
    getSelectedMedia(): Element[] {
        return ElementService.getSelectedElements(this.state.media_all)
    }
}

export const elementsStore: ElementsStore = new ElementsStore()
