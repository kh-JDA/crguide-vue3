import { Store } from "./main"
import { IMediaAndMaterialModel } from "../definitions/IMediaAndMaterialModel"
import { IMaterialMediumReaction } from "../definitions/IMaterialMediumReaction"

interface Reactions extends Object {
    reactions_all: IMaterialMediumReaction[]
}

class ReactionsStore extends Store<Reactions> {
    protected data(): Reactions {
        return {
            reactions_all: [],
        }
    }

    async getReactions(payload: IMediaAndMaterialModel): Promise<void> {
        const data = await this.fetchReactions(payload)
        this.state.reactions_all = data
    }

    async fetchReactions(payload: IMediaAndMaterialModel): Promise<IMaterialMediumReaction[]> {
        const url = "https://resistanceguide.azurewebsites.net/api/MaterialReactions/GetMaterialReactions"
        const response = await fetch(url, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                Materials: payload.Materials.map((item) => item.id),
                Media: payload.Media.map((item) => item.id),
            }),
        })
        const data: IMaterialMediumReaction[] = await response.json()
        return data
    }

    clearAllReactions(): void {
        this.state.reactions_all = []
    }
}

export const reactionsStore: ReactionsStore = new ReactionsStore()
