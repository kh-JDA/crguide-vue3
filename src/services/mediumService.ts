import { Element } from "../classes/Element"
import { IRawMedium } from "../definitions/IRawMedium"
export interface IMediumService {
    normalizeMedia(rawMedia: IRawMedium[]): Element[]
}

export const MediumService: IMediumService = {
    normalizeMedia(rawMedia: IRawMedium[]): Element[] {
        return rawMedia.map((rawMedium) => new Element(rawMedium.MediumId, rawMedium.MediumNameEnglish)) ?? []
    },
}
