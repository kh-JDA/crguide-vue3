import { Element } from "../classes/Element"
import { IRawMaterial } from "../definitions/IRawMaterial"

export interface IMaterialService {
    normalizeMaterials(rawMaterials: IRawMaterial[]): Element[]
}

export const MaterialService: IMaterialService = {
    normalizeMaterials(rawMaterials: IRawMaterial[]): Element[] {
        return (
            rawMaterials.map((rawMaterial) => {
                const el = new Element(rawMaterial.MaterialId, rawMaterial.MaterialNameEnglish)
                el.shortName = rawMaterial.MaterialShortName
                return el
            }) ?? []
        )
    },
}
