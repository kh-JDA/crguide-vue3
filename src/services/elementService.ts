import { Element } from "../classes/Element"

export interface IElementService {
    changeSelectedValueOfElement(allElements: Element[], elementIdToChange: number, selected: boolean): void
    getElementById(allElements: Element[], elementId: number): Element | undefined
    changeSelectionOfAllElements(allElements: Element[], selected: boolean): void
    getSelectedElements(allElements: Element[]): Element[]
}

export const ElementService: IElementService = {
    changeSelectedValueOfElement(allElements: Element[], elementIdToChange: number, selected: boolean): void {
        const elementToChange = allElements.find((item) => item.id == elementIdToChange)
        if (!elementToChange) {
            throw new Error(`Can't find element with id: ${elementIdToChange}`)
        }
        elementToChange.selected = selected
    },

    getElementById(allElements: Element[], elementId: number): Element | undefined {
        return allElements.find((item) => item.id == elementId)
    },

    changeSelectionOfAllElements(allElements: Element[], selected: boolean): void {
        allElements.forEach((item) => (item.selected = selected))
    },

    getSelectedElements(allElements: Element[]): Element[] {
        return allElements.filter((item) => item.selected)
    },
}
