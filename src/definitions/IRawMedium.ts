export interface IRawMedium {
    MediumId: number
    MediumNameEnglish: string
    MediumNameGerman: string
}
