import { Element } from "../classes/Element"

export interface IElementSelectedModel {
    element: Element
    selected: boolean
}
