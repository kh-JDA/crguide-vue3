import { IMaterialReaction } from "./IMaterialReaction"

export interface IMaterialMediumReaction {
    MaterialReactionId: number
    MaterialReactions: IMaterialReaction[]
    MediumId: number
    Reaction: number
    Temperature: number
}
