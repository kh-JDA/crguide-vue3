import { Element } from "../classes/Element"

export interface IMediaAndMaterialModel {
    Media: Element[]
    Materials: Element[]
}
