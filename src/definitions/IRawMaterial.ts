export interface IRawMaterial {
    MaterialId: number
    MaterialNameEnglish: string
    MaterialNameGerman: string
    MaterialShortName: string
}
