/* eslint-disable no-undef */
module.exports = {
    plugins: [
        require("tailwindcss"),
        require("postcss-nested"),
        require("autoprefixer"),
    ],
}
